<?php
    require_once 'Paginator.class.php';
    require_once 'class-search.php'; 
        
        
    function get_results_num($conn, $search_term)
    {
        if ($search_term == "all")
        {
            $query = "
                     SELECT COUNT(LEAF_CERT_ID) as Total
                     FROM
                     BLACKLIST";
        }
        else
        {
            $query = "
                      SELECT  COUNT(LEAF_CERT_ID) as Total
                      FROM BLACKLIST 
                      WHERE 
                          SUBJECT LIKE '%{$search_term}%'
                          OR ISSUER LIKE '%{$search_term}%'
                          OR SERIAL_NUMBER LIKE '%{$search_term}%'";
        }
        $result = mysqli_query($conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row['Total'];             
    }
    // $conn       = new mysqli( 'localhost', 'root', '', 'BLACKLIST' );
    $conn       = new mysqli( 'r2d4.imdeasoftware.org', 'malsign_website', 'SwjVqAyl5lduIUrwB4z93mg', 'MALSIGN_BLACKLIST' );
    $limit      = ( isset( $_GET['limit'] ) ) ? $_GET['limit'] : 50;
    $page       = ( isset( $_GET['page'] ) ) ? $_GET['page'] : 1;
    $links      = ( isset( $_GET['links'] ) ) ? $_GET['links'] : 5;
    $search_term= ( isset( $_GET['s'] ) ) ? $_GET['s'] : "all";
    
    if ($search_term == "all")
    {
        $query = "SELECT 
                      SUBJECT, ISSUER, SUBJECT_CN, SUBJECT_O, PEM_HASH, 
                      ISSUER_CN, ISSUER_O, SERIAL_NUMBER,F_SHA256, 
                      NOT_VALID_BEFORE, NOT_VALID_AFTER, REVOCATION 
                  FROM BLACKLIST ";
    }
    else
    {    
        $search_term = $conn->real_escape_string($search_term);
        $query = "SELECT  * 
                  FROM BLACKLIST 
                  WHERE 
                    SUBJECT LIKE '%{$search_term}%' 
                    OR ISSUER LIKE '%{$search_term}%'
                    OR SERIAL_NUMBER LIKE '%{$search_term}%'";
    }
    $Paginator  = new Paginator( $conn, $query);
    $results    = $Paginator->getData( $limit, $page );
    $res_total  = get_results_num($conn, $search_term);
    $results->res_total = $res_total; 
?>
