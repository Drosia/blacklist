  <!DOCTYPE html>
    <html>
      <head>
          <title> Malsign Blacklist</title>
          <link rel="stylesheet" href="css/bootstrap.min.css">
          <script src="js/ie-emulation-modes-warning.js"></script>
          <script src="js/jquery.min.js"></script>        
          <script src="js/bootstrap.min.js"></script>
          <script src="js/url.min.js"></script>        
          <link rel="stylesheet" href="css/customized.css">
          <link rel="stylesheet" href="css/bootstrap-responsive.css">
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1">
           

          <script type="text/javascript">
              $(document).ready(function() {
                  $page = url("file");
                  if(!$page){
                      $page = 'index.php';
                  }
                  $('#navigation li a').each(function(){
                      var $href = $(this).attr('href');
                      // alert($(this).parent().get(0).tagName);
                      var $li = $(this).parent()
                      if (($href == $page) || ($href == '')){
                           $li.addClass('active');
                      }
                      else {
                          $li.removeClass('active');
                      }
                  });
              });

          </script>
      </head>
