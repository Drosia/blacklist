<?php

class search {
  /**
   * MySQLi connection
   * @access private
   * @var object
   */
  private $mysqli;
  
        private $_query;
        private $_conn;
        private $rs;
  /**
   * Constructor
   *
   * This sets up the class
   */
  public function __construct($conn, $query) {
    // Connect to our database and store in $mysqli property
    $this->_conn = $conn;
    $this->_query = $query;

    $this->rs= $this->_conn->query( $this->_query );
  }
  
  /**
   * Search routine
   * 
   * Performs a search
   * 
   * @param string $search_term The search term
   * 
   * @return array/boolen $search_results Array of search results or false
   */
  public function search() {
    // Sanitize the search term to prevent injection attacks
    
    // Check results
    if ( ! $this->rs->num_rows ) {
      return false;
    }
    
    // Loop and fetch objects
    while( $row = $this->rs->fetch_object() ) {
      $rows[] = $row;
    }
    
    // Build our return result
    $search_results = array(
      'count' => $this->rs->num_rows,
      'results' => $rows,
    );

    return $search_results;
  }
}