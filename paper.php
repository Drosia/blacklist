  <div class="content">
    <div class="container">
      <div class="row-fluid">
        <div class="offset1 span10 white-panel title">
          <div class="hometitle">
            <h3> Certified PUP: Abuse in Authenticode Code Signing</h3>

            <p class="paperauthor"><a href="http://software.imdea.org/people/platon.kotzias/index.html">Platon Kotzias</a>,
						<a href="http://software.imdea.org/people/srdjan.matic/index.html">Srdjan Matic</a>, 
						<a href="http://software.imdea.org/people/richard.rivera/index.html">Richard Rivera</a> 
            , and <a href="http://software.imdea.org/~juanca/">Juan Caballero</a></p>

            <p class="paperauthor">In Proceedings of the <a href=
            "https://www.usenix.org/conference/usenixsecurity13">22nd ACM Conference on Computer and Communications Security</a>, October 2015.</p>
          </div>
        </div>
      </div>
      <div class="row-fluid">
        <div class="offset1 span10 white-panel nontitle">
          <div class="abstract2">
              <div class="row-fluid">
                <div class="span7">
                  <h4>Abstract</h4>
                  <p class="abstract">
									Code signing is a solution to verify the integrity of software and 
									its publisher's identity, but it can be abused by malware 
                  and potentially unwanted programs (PUP) to look benign.
                  This work performs a systematic analysis of Windows Authenticode code signing
                  abuse, evaluating the effectiveness of existing defenses by certification 
                  authorities. 
                  We identify a problematic scenario in Authenticode where 
                  timestamped signed malware successfully validates even after 
                  the revocation of their code signing certificate.
                  We propose hard revocations as a solution.
                  We build an infrastructure that automatically analyzes potentially malicious
                  executables, selects those signed, clusters them into operations,
                  determines if they are PUP or malware, and produces a certificate blacklist.
                  We use our infrastructure to evaluate 356K samples from 2006-2015.
                  Our analysis shows that most signed samples are PUP (88%-95%) and 
                  that malware is not commonly signed (5%-12%). 
                  We observe PUP rapidly increasing over time in our corpus. 
                  We measure the effectiveness of CA defenses such as identity checks and 
                  revocation, finding that 99.8% of signed PUP and 37% of signed malware 
                  use CA-issued certificates and only 17% of malware certificates and 
                  15% of PUP certificates have been revoked. 
                  We observe most revocations lack an accurate revocation reason.
                  We analyze the code signing infrastructure of the 10 largest PUP operations 
                  exposing that they heavily use file and certificate polymorphism and 
                  that 7 of them have multiple certificates revoked.
                  Our infrastructure also generates a certificate blacklist 
                  9x larger than current ones.

                  </p>
                  <h4>Download</h4>
                  <p class="abstract">Download the <a href="malsign_paper.pdf">conference version</a> of the paper.</p>
                </div>
                <div class="span4">
                  <a href="malsign_paper.pdf"><img src="img/paper_cover.png" style="border:1px solid #aaa; margin-left:40px; margin-right:40px; margin-top:80px; margin-bottom:10px;" class="displayed"/></a>
                </div>
              </div>
              </div>
              </div>
              <div class="row-fluid">
              					<div class="offset1 span10 white-panel">
              						<div class="abstract2">
              						  <h4>Bibtex</h4>
              						  <div class="indent"><pre>
@inproceedings{malsign,
  author = {Platon Kotzias and Srdjan Matic and Richard Rivera and Juan Caballero},
  title = {{Certified PUP: Abuse in Authenticode Code Signing}},
  booktitle = {Proceedings of the 22nd ACM Conference on Computer and Communication Security},
  address = {Denver, CO},
  month = {October},
  year = {2015},
  butype = {inproceedings},
  buconfj = {ccs},
  monthno = {10}
}
</div></div>
              					</div>
              				</div>
          </div>
        </div>
      </div>
