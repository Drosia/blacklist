  <div class="container">
    
    <div class="col-md-10 col-md-offset-1 white-box box-text">
      <h2> <b>Collection Method</b> </h2>
            <p class="normal">
            The certificate list below is one of the outputs of our Code Signing 
      analysis infrastructure. 
            We used 350K unlabeled samples from malware repositories,
            mainly <a href="http://virusshare.com/">VirusShare</a>
      as input to our infrastructure.
            Then, it uses VirusTotal to discard any samples that are flagged by less 
      than 4 AV engines.
      The list contains all the properly CA-issued certificates in our datasets. 
            More details on our collection and analysis can be found in our <a href="index.php">paper</a>.
      </p>
    </div>
    <div class="col-md-10 col-md-offset-1 white-box box-text" style="margin-top:10px;">
      <h2> <b>Download</b> </h2>
            <p class="normal">
            The certificate collection can be downloaded from <a href="certs_in_der.zip">here</a>.
      All certificates are in DER format.
            You can examine a certificate using the OpenSSL suite. For example:
      <pre> openssl x509 -inform DER -text -in 001926795f482a11cf7d39351111296a.der </pre>
      </p>
    </div>
    <div class="col-md-10 col-md-offset-1">
    <div id="over_table">
      <div id="up">
        <h2 id="title_of_table"><b>Certificates</b></h2>
<!--           <label id="searchlabel">Search</label> -->

      </div>
      <div id="middle" >
       <label id="result_count"> 
        <div class="results-count">
          <p><?php echo $results->res_total; ?> certs found</p>
        </div>
       </label>
      </div>
   
       <div class="paginate">
         <?php echo $Paginator->createLinks( $links, $limit, $page, $search_term, 'pagination pagination-sm' ); ?>
       </div>
   
        <div id="custom-search-input" class="navbar-collapse collapse navbar-right">
          <div class="input-group col-md-12">
            <form action="" method="get">
              <input type="search" name="s" class="  search-query form-control" placeholder="Search" 
                     value="<?php if ($search_term != "all") {echo $search_term;} ?>"/>
              <span class="input-group-btn">
                <button class="btn btn-danger" type="submit"></button>
              </span>
            </form>
          </div>
        </div> 
       </div>
      <table class="table-fill">
       <thead>
         <tr>
          <th class='text-center'>Subject</th>
          <th class='text-center'>Issuer</th>
          <th class='text-center'>Pem Hash</th>
          <th class='text-center'>Serial Number</th>
          <th class='text-center'>Validity Period</th>
          <th class='text-center'>Revocation</th>
          <th class='text-center'>VirusTotal</th>
          </tr>
        </thead>
        <tbody class="table-hover">           

          <?php for( $i = 0; $i < count( $results->data ); $i++ ) : ?>
          <tr>
            <td class="text-center">
              <span>CN:</span> <?php echo $results->data[$i]['SUBJECT_CN'];?> 
              <br><span>O:</span> <?php echo $results->data[$i]['SUBJECT_O'];?>
            </td>
            <td class="text-center">
              <span>CN:</span> <?php echo $results->data[$i]['ISSUER_CN'];?>
              <br><span>O:</span> <?php echo $results->data[$i]['ISSUER_O'];?>
            </td>
            <td class="text-center"><?php echo $results->data[$i]['PEM_HASH']; ?></td>
            <td class="text-center"><?php echo $results->data[$i]['SERIAL_NUMBER']; ?></td>
            <td class="text-center"> 
                </span><?php echo substr($results->data[$i]['NOT_VALID_BEFORE'],0,10);?> - 
                </span> <?php echo substr($results->data[$i]['NOT_VALID_AFTER'],0,10);?>
            </td>
            <?php 
               $revocation = $results->data[$i]['REVOCATION'];
               if (empty($revocation)){ $revocation = "-";}
            ?>
            <td class="text-center"><?php echo $revocation;?></td>
            <td class="text-center">
              <a href= <?php echo "https://www.virustotal.com/en/file/".$results->data[$i]['F_SHA256']."/analysis"; ?>>
              <img IMG HEIGHT="15" WIDTH="15"  src="/blacklist/img/109-External-512.png" border="0"></a>
            </td> 
          </tr>
          <?php endfor; ?>
        </tbody>
      </table> 

    <div class="paginate">
      <?php echo $Paginator->createLinks( $links, $limit, $page, $search_term, 'pagination pagination-sm' ); ?>
    </div>

  </div>
 </div>
